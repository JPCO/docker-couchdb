#!/bin/bash

set -e
if [[ ! -e /usr/local/lib/couchdb/etc/local.d/docker.ini ]]; then
  printf "[admins]\n$COUCHDB_USER = $COUCHDB_PASSWORD\n\n" > /usr/local/lib/couchdb/etc/local.d/docker.ini
  UUID=$(cat /proc/sys/kernel/random/uuid)
  UUID2=$(cat /proc/sys/kernel/random/uuid)
  printf "[cluster]\nn = 1\n\n" >> /usr/local/lib/couchdb/etc/local.d/docker.ini
  printf "[chttpd]\nbind_address = any\n\n" >> /usr/local/lib/couchdb/etc/local.d/docker.ini
  printf "[httpd]\nbind_address = any\n\n" >> /usr/local/lib/couchdb/etc/local.d/docker.ini
  printf "[couchdb]\nuuid = $UUID\n\n" >> /usr/local/lib/couchdb/etc/local.d/docker.ini
  printf "[couch_httpd_auth]\nsecret = $UUID2\n" >> /usr/local/lib/couchdb/etc/local.d/docker.ini
fi

chown couchdb:couchdb /usr/local/lib/couchdb/etc/local.d/*.ini
chown -R couchdb:couchdb /usr/local/lib/couchdb/data
exec sudo -HE -u couchdb "$@"
